package com.megastore.megastore.controller;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.megastore.megastore.model.Store;
import com.megastore.megastore.repository.StoreRepository;

import javassist.NotFoundException;
import javassist.tools.web.BadHttpRequest;

@RestController
public class StoreController {

	@Autowired
	private StoreRepository repository;

	@GetMapping("/stores")
	public List<Store> retrieveStoreByName(@RequestParam(required=false) String name) throws NotFoundException {

		if(name == null) {
			return repository.findAll();
		}else {
			List<Store> stores = repository.findAllByName(name);

			if(stores.isEmpty())
				throw new NotFoundException("Not found any Store with this name!");
			return stores;
		}

	}

	@GetMapping("/stores/{id}")
	public Store retrieveStoreByUuid(@Valid @PathVariable String id) throws BadHttpRequest, NotFoundException {
		
		Store store = repository.findOneByUuid(UUID.fromString(id));
		
		if (store == null)
		      throw new NotFoundException("id-" + id);

		return store;
	}

	@PostMapping("/stores")
	@ResponseBody
	public Store createStore(@Valid @RequestBody Store store) {
		
		Store savedStore = repository.save(store);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{Uuid}")
				.buildAndExpand(savedStore.getUuid()).toUri();

		return new ResponseEntity<Store>(store, HttpStatus.CREATED).getBody();
		
	}

	@PutMapping("/stores/{id}")
	public Store updateStore(@RequestBody Store store, @PathVariable String id) throws NotFoundException {

		Store storeOptional = repository.findOneByUuid(UUID.fromString(id));

		if (storeOptional == null)
			throw new NotFoundException("Not found data with "+id+" Uuid");

		store.setUuid(UUID.fromString(String.valueOf(id)));

		return repository.save(store);

	}

	@DeleteMapping("/stores/{id}")
	public void deleteStore(@PathVariable String id) {
		repository.deleteByUuid(UUID.fromString(id));
	}

}
