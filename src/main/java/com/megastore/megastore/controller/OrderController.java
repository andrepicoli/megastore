package com.megastore.megastore.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.NotAcceptableStatusException;

import com.megastore.megastore.model.Order;
import com.megastore.megastore.repository.OrderRepository;

import javassist.NotFoundException;
import javassist.tools.web.BadHttpRequest;

@RestController
public class OrderController {

	@Autowired
	private OrderRepository repository;

	@GetMapping("/orders")
	public List<Order> retrieveOrderByStatus(@RequestParam(required=false) String status) throws NotFoundException {

		if(status == null) {
			return repository.findAll();
		}else {
			List<Order> orders = repository.findAllByStatus(status);

			if(orders.isEmpty())
				throw new NotFoundException("Not found any order with this name!");
			return orders;
		}

	}

	@GetMapping("/orders/{id}")
	public Order retrieveOrder(@PathVariable String id) throws BadHttpRequest, NotFoundException {

		if(id == null || id.equals("")){
			throw new BadHttpRequest();
		}

		Order order = repository.findOneByUuid(UUID.fromString(id));

		if (order == null)
			throw new NotFoundException("id-" + id);

		return order;
	}

	@PostMapping("/orders")
	public Order createOrder(@RequestBody Order order) {

		Date date = new Date();
		order.setConfirmationDate(date);

		return repository.save(order);
	}

	@PutMapping("/orders/{id}")
	public Order updateOrder(@Valid @RequestBody Order order, @PathVariable String id) throws NotFoundException {

		Order orderOptional = repository.findOneByUuid(UUID.fromString(id));

		Date date = new Date();
		orderOptional.setConfirmationDate(date);

		if (orderOptional == null)
			throw new NotFoundException("Not found data with "+id+" Uuid");

		order.setUuid(UUID.fromString(String.valueOf(id)));

		return repository.save(order);

	}

	@PutMapping("/orders/refund/{id}")
	public Order refundOrder(@PathVariable String id) {

		Order orderOptional = repository.findOneByUuid(UUID.fromString(id));

		Date current = new Date();
		
		long diff = current.getTime() - orderOptional.getConfirmationDate().getTime();
		
		diff = (diff / (1000 * 60 * 60 * 24));
		
		if(diff <= 10) {
			orderOptional.setStatus("canceled");
			orderOptional.getPayment().setStatus("refunded");
			repository.save(orderOptional);
		}else {
			throw new NotAcceptableStatusException("Order has more than 10 days! Refund is not allowed!");
		}
		
		return orderOptional;
	}


	@DeleteMapping("/orders/{id}")
	public void deleteOrder(@PathVariable String id) {
		repository.deleteByUuid(UUID.fromString(id));
	}

}
