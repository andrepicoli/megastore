package com.megastore.megastore.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="store")
public class Store {
	
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
		name = "UUID",
		strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "id", updatable = false, nullable = false)
    private UUID uuid;

	@Column(name="name")
	@NotNull(message="Parameter name can't be Null!")
	@NotEmpty(message="Missing value in parameter 'name'")
	private String name;
	
	@Column
	@NotNull(message="Parameter address can't be Null!")
	@NotEmpty(message="Missing value in parameter 'address'")
	private String address;
	
	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
}
