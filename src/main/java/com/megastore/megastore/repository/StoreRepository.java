package com.megastore.megastore.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.megastore.megastore.model.Store;

@Repository
public interface StoreRepository extends JpaRepository<Store, String> {
	
	Store findOneByUuid(UUID uuid);
	
	List<Store> findAllByName(String name);
	
	@Transactional
	@Modifying
	@Query("delete from Store where uuid = :uuid")
	void deleteByUuid(@Param("uuid") UUID uuid);
	
}