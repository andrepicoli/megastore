package com.megastore.megastore.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.megastore.megastore.model.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, String>{
	
	Order findOneByUuid(UUID uuid);
	
	List<Order> findAllByStatus(String status);

	@Transactional
	@Modifying
	@Query("delete from Order where uuid = :uuid")
	void deleteByUuid(@Param("uuid") UUID uuid);
	
}
