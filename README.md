#MegaStore API

This project was created to show my skills.

I didn't have so much time to create a complete API with Token, AWS EC2 Instance and Docker File... but I know how to do.

To start this API, first of all, your need to clone this project in the following link

## git clone git@bitbucket.org:andrepicoli/megastore.git ##

After that, install all maven dependencies

## mvn clean install -DskipTests=true ##
and
## mvn eclipse\:eclipse ##

Third step is creating a PostgreSQL database with name 'megastore', the tables will be created automatically by Hibernate.

To run spring-boot, do the following code

## mvn spring-boot:run ##

Now the API is running...

Based on the challenge, I created a script to show what I did.

You can use the json examples to test...

### 1º Creating a Store ###
POST Method - http://localhost:8080/stores
{
	"name":"MegaStore",
	"address":"99 Avenue, 192, NY"
}

### 2º Updating a Store Information ###
PUT Method - http://localhost:8080/stores/{uuid}
{
	"name":"MegaStore",
	"address":"99 Street, 193, NY"
}

### 3º Retrieve a Store by Parameters ###
GET Method - http://localhost:8080/stores?name=MegaStore
{
	"name":"MegaStore",
	"address":"99 Street, 193, NY"
}

### 4º Creating an Order with Items and Payment ###
POST Method - http://localhost:8080/orders
{
	"confirmationDate": "2019-01-01",
	"address":"99 Street",
	"status":"confirmed",	
	"payment":{
		"status":"approved",
		"creditCardNumber":"0493049382937472",
		"paymentDate":"2019-03-01"
	},
	"items":[
		{
			"description":"Rice",
			"unitPrice":"10.80",
			"quantity":"10"
		},
		{
			"description":"Beans",
			"unitPrice":"8.30",
			"quantity":"10"
		},{
			"description":"Tomato",
			"unitPrice":"3.88",
			"quantity":"10"
		}
	]
}

### Retrieve a Order by Status
GET Method - http://localhost:8080/orders?status=confirmed
[    {
        "uuid": "b3a1ad54-6bce-4be4-ae77-e72c504638ab",
        "address": "99 Street",
        "confirmationDate": "2019-02-03",
        "status": "confirmed",
        "payment": {
            "uuid": "55ca5b8e-3c6c-4ea9-82e7-b851c41cb67d",
            "status": "approved",
            "creditCardNumber": "0493049382937472",
            "paymentDate": "2019-02-28"
        },
        "items": [
            {
                "uuid": "6c4b73c3-f210-44a2-8a14-6b7f1291f59f",
                "description": "Rice",
                "unitPrice": 10.8,
                "quantity": 10
            },
            {
                "uuid": "cdd248f8-9d14-4f38-9f0c-1867e5722e13",
                "description": "Beans",
                "unitPrice": 8.3,
                "quantity": 10
            },
            {
                "uuid": "02f2c72d-ae3c-49cd-a5b6-d010497243be",
                "description": "Tomato",
                "unitPrice": 3.88,
                "quantity": 10
            }
]

### Refund a Order ###
PUT Method - http://localhost:8080/orders/refund/{uuid}


If I had more time, I intend to do:

- Create Token validation (JWT)

- Create Quality Assurance using JUnit and RestAssured

- Create a Docker Image FIle

- Create AWS EC2 Instance

- Upload Docker Image by AWS SDK on EC2



Thanks for the opportunity! 




